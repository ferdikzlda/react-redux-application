import { all } from 'redux-saga/effects';
import movieListSaga from './containers/Movie/List/saga';
import movieDetailSaga from './containers/Movie/Detail/saga';

export default function* sagas() {
  yield all([
    movieListSaga(),
    movieDetailSaga(),
  ]);
}
