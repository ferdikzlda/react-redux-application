import movieListReducer from './containers/Movie/List/reducer';
import movieDetailReducer from './containers/Movie/Detail/reducer';

const reducers = {
  movieListReducer,
  movieDetailReducer,
}
export default reducers;
