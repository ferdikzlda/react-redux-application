import React, { Component } from 'react';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import { Row, Col } from 'react-bootstrap';
import Layout from 'components/Layout';
import Style from './style.module.scss';
import PropTypes from 'prop-types';

class MovieDetail extends Component {
  static propTypes = {
    currentMovie: PropTypes.object,
  }

  render() {
    const { currentMovie } = this.props;
    return (
      <Layout>
        <Row>
          <Col md={6} xs={6}>
            <img src={currentMovie.Poster} className={Style.detailImage} alt={currentMovie.Title} />
          </Col>
          <Col md={6} xs={6}>
            <div className={Style.detailFrame}>
              <h1>{currentMovie.Title}</h1>
              <h2>{currentMovie.Type}</h2>
              <h2>{currentMovie.Year}</h2>
            </div>
          </Col>
        </Row>
      </Layout>
    );
  }
}


const mapStateToProps = (state) => {
  return {
    currentMovie: state.movieListReducer.currentMovie,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    homePage: () => {
      dispatch(push('/'))
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(MovieDetail);