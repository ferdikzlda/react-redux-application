import React, { Component } from 'react';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import { loadMovies, setCurrentMovie, searchMovies } from './action';
import { Row, Col } from 'react-bootstrap';
import './style.css';
import Layout from 'components/Layout';
import PropTypes from 'prop-types';

class MovieList extends Component {
  constructor(props) {
    super(props);
    this.searchScroll = this.searchScroll.bind(this);
  }

  static propTypes = {
    searchParams: PropTypes.object,
    searchMovies: PropTypes.func,
    movies: PropTypes.array,
    selectMovie: PropTypes.func,
  }

  componentDidMount() {
    document.addEventListener('scroll', this.searchScroll);
  }

  componentWillUnmount() {
    document.removeEventListener('scroll', this.searchScroll);
  }

  searchScroll() {
    const { searchParams, searchMovies } = this.props;
    const d = document.documentElement;
    const offset = d.scrollTop + window.innerHeight;
    const height = d.offsetHeight;
    let params = searchParams;
    if (offset === height) {
      params = { ...params, page: searchParams.page + 1 }
      searchMovies(params);
    }
  }

  render() {
    const { movies, selectMovie } = this.props;
    return (
      <Layout>
        {
          movies.length === 0 ? (
            <div className="result">No results</div>
          ) : ('')
        }
        <Row>
          {
            movies.map((movie, key) => (
              <Col key={key} md={4} xs={6}>
                <div className='frame' onClick={() => selectMovie(movie)}>
                  <img src={movie.Poster} className='image' alt={movie.Title} />
                  <div className='text-center'>{movie.Title}</div>
                </div>
              </Col>
            ))
          }

        </Row>
      </Layout>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    movies: state.movieListReducer.movies,
    searchParams: state.movieListReducer.searchParams,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    loadMovies: () => {
      dispatch(loadMovies());
    },
    selectMovie: (movie) => {
      dispatch(setCurrentMovie(movie));
      dispatch(push(`/movie/${movie.imdbID}`))
    },
    searchMovies: (params) => {
      dispatch(searchMovies(params));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(MovieList);