import { LOADED_MOVIES, SET_CURRENT_MOVIE, SEARCH_MOVIES, CLEAR_MOVIES } from './constant';

const reducer = (state = { movies: [], currentMovie: {}, searchParams: { s: '', page: 1, type: 'new' } }, action) => {
  switch (action.type) {
  case LOADED_MOVIES:
    return { ...state, movies: [...state.movies, ...action.value] };
  case SET_CURRENT_MOVIE:
    return { ...state, currentMovie: action.value };
  case SEARCH_MOVIES:
    return { ...state, searchParams: action.value };
  case CLEAR_MOVIES:
    return { ...state, movies: [] };
  default:
    return state;
  }
};

export default reducer;