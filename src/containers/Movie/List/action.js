import { LOAD_MOVIES, LOADED_MOVIES, SEARCH_MOVIES, SET_CURRENT_MOVIE, CLEAR_MOVIES } from './constant';

export function loadMovies() {
  return {
    type: LOAD_MOVIES,
  };
}

export function loadedMovies(data) {
  return {
    type: LOADED_MOVIES,
    value: data,
  };
}

export function searchMovies(data) {
  return {
    type: SEARCH_MOVIES,
    value: data,
  };
}

export function setCurrentMovie(data) {
  return {
    type: SET_CURRENT_MOVIE,
    value: data,
  }
}

export function clearMovies() {
  return {
    type: CLEAR_MOVIES,
  }
}