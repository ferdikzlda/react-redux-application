import { put, takeLatest } from 'redux-saga/effects';
import { push } from 'react-router-redux';
import { loadedMovies } from './action';
import { LOAD_MOVIES, SEARCH_MOVIES } from './constant';

function* loadMoviesSaga() {

}

function* searchMoviesSaga(action) {

  const data = yield fetch(`http://www.omdbapi.com/?apikey=fec5d535&s=${action.value.s}&page=${action.value.page}`)
    .then(response => response.json())
    .then((response) => {
      return response.Error ? [] : response.Search;
    })
    .catch((error) => {
      // eslint-disable-next-line no-console
      console.log('Request failed', error);
    })

  yield put(loadedMovies(data));
  yield put(push('/movies'));
}

export default function* movieListSaga() {
  yield takeLatest(LOAD_MOVIES, loadMoviesSaga)
  yield takeLatest(SEARCH_MOVIES, searchMoviesSaga)
}