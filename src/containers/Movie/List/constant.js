export const LOAD_MOVIES = 'Movie/List/LOAD_MOVIES';
export const LOADED_MOVIES = 'Movie/List/LOADED_MOVIES';
export const SEARCH_MOVIES = 'Movie/List/SEARCH_MOVIES';
export const SET_CURRENT_MOVIE = 'Movie/List/SET_CURRENT_MOVIE';
export const CLEAR_MOVIES = 'Movie/List/CLEAR_MOVIES';
