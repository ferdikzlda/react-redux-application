import React, { Component } from 'react';
import Layout from 'components/Layout';
import Style from './style.module.scss';

class App extends Component {
  render() {
    return (
      <Layout>
        <h1 className={Style.title}>Welcome</h1>
      </Layout>
    );
  }
}

export default App;
