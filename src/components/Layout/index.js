import React, { Component } from 'react';
import { Container } from 'react-bootstrap';
import Header from 'components/Header';
import Footer from 'components/Footer';
import PropTypes from 'prop-types';

export default class Layout extends Component {
  static propTypes={
    children: PropTypes.node,
  }

  render() {
    const { children }=this.props;
    return (
      <Container>
        <Header/>
        {children}
        <Footer/>
      </Container>
    );
  }
}
