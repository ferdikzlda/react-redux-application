import React, { Component } from 'react';
import { connect } from 'react-redux';
import { NavLink } from 'react-router-dom'
import { Navbar, Nav, Form, FormControl } from 'react-bootstrap';
import { searchMovies, clearMovies } from 'containers/Movie/List/action';
import PropTypes from 'prop-types';

class CustomNavbar extends Component {
  static propTypes={
    searchMovies: PropTypes.func,
  }

  render() {
    const {searchMovies}=this.props;
    return (
      <Navbar className="bg-light justify-content-between">
        <Nav className="mr-auto">
          <NavLink to='/movies'>Movies</NavLink>
        </Nav>
        <Form inline>
          <FormControl onChange={searchMovies} type="text" placeholder="Search" className=" mr-sm-2" />
        </Form>
      </Navbar>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    movies: state.movieListReducer.movies,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    searchMovies: (e) => {
      if (e.target.value.length >= 3) {
        const params = {
          s: e.target.value,
          page: 1,
          type: 'new',
        };
        setTimeout(() => {
          dispatch(clearMovies());
          dispatch(searchMovies(params))
        }, 300);
      }
      e.preventDefault();
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(CustomNavbar);
