import React, { Component } from 'react';
import Style from './style.module.scss';

export default class Footer extends Component {
  render() {
    return (
      <div className={Style.frame}>
        <div className={Style.leftSection}>
          <li>İtem</li>
          <li>İtem</li>
          <li>İtem</li>
        </div>
        <div className={Style.rightSection}>
          <li>İtem</li>
          <li>İtem</li>
          <li>İtem</li>
        </div>
      </div>
    );
  }
}